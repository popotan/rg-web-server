# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request, Request, jsonify, session, redirect, g
import json
import urllib
import os
from datetime import datetime, timedelta, time

from sqlalchemy import and_, or_, text, asc, desc

from app import app, db

from app.model.domain.domain import domain_orm
from app.model.domain.allow_host import allow_host_orm

from app.model.guest.guest import guest_orm
from app.model.guest.guest_log import guest_log_orm

from app.model.rg_popup.rg_popup import rg_popup_orm

from app.config.static import *

rg_popup = Blueprint('rg_popup', __name__)


@rg_popup.route('', methods=['GET'])
def get_popup_list():
    host = request.headers['Host']

    host_info = allow_host_orm().query.filter_by(
        host_name=host).filter_by(status='usable').first()
    if host_info:
        lang_code = request.args.get('lang_code', str)
        popup_list = rg_popup_orm().query.filter_by(lang_code=lang_code).all()

        popup_result = []
        for popup in popup_list:
            popup_result.append({
                'id': popup.id,
                'title': popup.title,
                'description': popup.description,
                'exposure_path': popup.exposure_path,
                'exposure_start_date': popup.exposure_start_date,
                'exposure_end_date': popup.exposure_end_date,
                'exposure_type': popup.exposure_type,
                'exposure_size': {
                    'width': popup.exposure_size_width,
                    'height': popup.exposure_size_height
                },
                'lang_code': popup.lang_code,
                'reg_date': popup.reg_date
            })
        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            },
            'response': popup_result
        })
    else:
        return '', 404
