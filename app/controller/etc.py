# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, request, Request, jsonify, session, redirect, g
import json
import urllib
import os
import re
from datetime import datetime, timedelta, time
import calendar

from sqlalchemy import and_, or_, text, asc, desc

from app import app, db

from app.model.domain.domain import domain_orm
from app.model.domain.allow_host import allow_host_orm
from app.model.domain.contact_us import contact_us_orm

from app.config.static import *

from app.composer.mail_sender import MailSender

etc = Blueprint('etc', __name__)


@etc.route('/contact_us', methods=['POST'])
def post_contanct_us():
    host = request.headers['ServiceDomain']
    host_info = allow_host_orm().query.filter(
        allow_host_orm.host_name.ilike(text('"%' + host + '%"'))).filter_by(status='usable').first()

    if host_info:
        article = contact_us_orm()
        article.domain_id = host_info.domain_id
        article.email = request.get_json()['email']
        article.title = request.get_json()['title']
        article.description = request.get_json()['description']
        db.session.add(article)
        db.session.commit()

        MailSender(id='admin@withasia.org', password="withasia2018")\
            .write(mail_from=request.get_json()['email'], mail_to='admin@withasia.org', subject='참여신청문의', msg=request.get_json()['description'])\
            .send(mail_from=request.get_json()['email'], mail_to='admin@withasia.org')
        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            }
        }), 201
    else:
        return '', 404


@etc.route('/contact_us/thehow', methods=['POST'])
def post_contact_us_thehow():
    host = request.headers['ServiceDomain']
    host_info = allow_host_orm().query.filter(
        allow_host_orm.host_name.ilike(text('"%' + host + '%"'))).filter_by(status='usable').first()

    if host_info:
        article = contact_us_orm()
        article.domain_id = host_info.domain_id
        article.email = request.get_json()['email']
        article.title = request.get_json()['title']
        article.description = request.get_json()['description']
        db.session.add(article)
        db.session.commit()

        MailSender(id='yesjyj63@gmail.com', password="withasia2018")\
            .write(mail_from=request.get_json()['email'], mail_to='yesjyj63@gmail.com', subject='지식공방 하우 문의사항', msg=request.get_json()['description'])\
            .send(mail_from=request.get_json()['email'], mail_to='yesjyj63@gmail.com')
        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            }
        }), 201
    else:
        return '', 404


@etc.route('/ogp', methods=['GET'])
def get_ogp_json():
    from app.composer.rg_board.og_parser import OgParser
    from urllib.parse import unquote_plus
    target_url = unquote_plus(request.args.get('url'))
    og = OgParser(target_url).json

    return jsonify({
        'meta': {
            'message': 'SUCCESS'
        },
        'response': og
    })
