# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, request, Request, jsonify, session, redirect, send_file, g
import json
import urllib
import os
import re
from datetime import datetime, timedelta, time
import calendar

from sqlalchemy import and_, or_, text, asc, desc

from app import app, db

from app.model.domain.domain import domain_orm
from app.model.domain.allow_host import allow_host_orm

from app.model.guest.guest import guest_orm
from app.model.guest.guest_log import guest_log_orm
from app.model.guest.facebook_session import facebook_session_orm

from app.model.rg_board.rg_board import rg_board_orm
from app.model.rg_board.rg_board_auth import rg_board_auth_orm
from app.model.rg_board.rg_board_lang_code import rg_board_lang_code_orm
from app.model.rg_board.rg_board_content import rg_board_content_orm
from app.model.rg_board.rg_board_content_history import rg_board_content_history_orm
from app.model.rg_board.rg_board_content_access_log import rg_board_content_access_log_orm
from app.model.rg_board.rg_board_file import rg_board_file_orm
from app.model.rg_board.rg_board_content_tag import rg_board_content_tag_orm

from app.config.static import *

rg_board = Blueprint('rg_board', __name__)


def had_auth():

    def is_himself():
        if 'guest_id' in session['stored'][g.serviceDomain]:
            return True
        else:
            return False

    def is_admin():
        if 'auth' in session['stored'][g.serviceDomain]:
            if 'admin' in session['stored'][g.serviceDomain]['auth']:
                return True
        return False

    if 'stored' in session:
        if g.serviceDomain in session['stored']:
            if is_himself() or is_admin():
                return True
    return False


def board_info(board_name):
    host = request.headers['ServiceDomain']
    if host:
        host_info = allow_host_orm().query.filter(
            allow_host_orm.host_name.ilike(text('"%' + host + '%"'))).filter_by(status='usable').first()
        if host_info:
            info = rg_board_orm().query.filter_by(
                domain_id=host_info.domain_id).filter_by(name=board_name).first()
            return info
        else:
            return None
    else:
        return None


@rg_board.route('', methods=['GET'])
def get_board_list():
    host = request.headers['ServiceDomain']
    host_info = allow_host_orm().query.filter(
        allow_host_orm.host_name.ilike(text('"%' + host + '%"'))).filter_by(status='usable').first()
    if host_info:
        board_list = rg_board_orm().query.filter_by(domain_id=host_info.domain_id).all()

        board_result = []
        for board in board_list:
            board_result.append({
                'name': board.name,
                'style': board.style,
                'lang_code': board.get_lang_code()
            })

        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            },
            'response': board_result
        })
    else:
        return '', 404


@rg_board.route('/<board_name>/info')
def get_board_info(board_name):
    info = board_info(board_name)
    if not info:
        return '', 404

    return jsonify({
        'meta': {
            'message': 'SUCCESS'
        },
        'response': {
            'name': info.name,
            'style': info.style,
            'lang_code': info.get_lang_code(),
            'write_auth': [board_auth.auth for board_auth in rg_board_auth_orm().query.filter_by(rg_board_id=info.id).all()]
        }
    })


@rg_board.route('/<board_name>', methods=['GET'])
def get_article_list(board_name):
    board = board_info(board_name)
    if not board:
        return '', 404

    page = int(request.args.get('page'))
    lang_code = request.args.get('lang')

    keyword = request.args.get(
        'keyword') if request.args.get('keyword') else ''
    keyword_type = request.args.get(
        'keyword_type') if request.args.get('keyword_type') else ''
    tag = request.args.get('tag') if request.args.get('tag') else ''
    limit = 10

    article_list = rg_board_content_orm().query

    article_list = article_list.filter_by(rg_board_id=board.id)\
        .filter_by(lang_code=lang_code)\
        .filter_by(status='visible')

    and_conditions = []
    or_conditions = []

    if keyword and keyword_type:
        if keyword_type == 'article':
            or_conditions.append(
                rg_board_content_orm.title.ilike(text('"%' + keyword + '%"')))
            or_conditions.append(
                rg_board_content_orm.description.ilike(text('"%' + keyword + '%"')))
        elif keyword_type == 'guest_name':
            or_conditions.append(
                rg_board_content_orm.guest_name.ilike(text('"%' + keyword + '%"')))

    article_list = article_list.filter(and_(*and_conditions))
    article_list = article_list.filter(or_(*or_conditions))

    if tag:
        article_list = article_list.outerjoin((rg_board_content_tag_orm, rg_board_content_tag_orm.rg_board_content_id ==
                                               rg_board_content_orm.id)).filter(rg_board_content_tag_orm.tag == tag).filter(rg_board_content_tag_orm.status == 'visible')

    article_total_count = article_list.count()
    article_list = article_list.order_by(
        desc(rg_board_content_orm.cid)).offset((page-1)*limit).limit(limit)
    article_list = article_list.all()

    result = []
    for article in article_list:
        view_count = rg_board_content_access_log_orm().query.filter_by(
            rg_board_content_id=article.id).count()

        file_list_result = []
        file_list = rg_board_file_orm().query.filter_by(
            rg_board_content_id=article.id).filter_by(status='visible').all()

        tag_list_result = []
        tag_list = rg_board_content_tag_orm().query.filter_by(
            rg_board_content_id=article.id).filter_by(status='visible').all()
        for tag_obj in tag_list:
            tag_list_result.append(tag_obj.tag)

        for ufile in file_list:
            file_list_result.append({
                'id': ufile.id,
                'filename': ufile.filename,
                'filetype': ufile.filetype,
                'filesize_mb': ufile.filesize_mb,
                'reg_date': ufile.reg_date
            })

        guest_info = guest_orm().query.filter_by(id=article.guest_id).first()
        profile_image_url = ''
        if guest_info:
            if guest_info.division == 'FACEBOOK':
                profile_image_url = facebook_session_orm().query.filter_by(
                    guest_id=guest_info.id).first().profile_image_url

        result.append({
            'cid': article.cid,
            'title': article.title,
            'description': article.description,
            'guest_id': article.guest_id,
            'guest_name': article.guest_name,
            'guest_profile_image': profile_image_url,
            'lang_code': article.lang_code,
            'view_count': view_count,
            'lang_code': article.lang_code,
            'in_private': article.in_private,
            'is_notice': article.is_notice,
            'popup': article.popup,
            'file_list': file_list_result,
            'tag_list': tag_list_result,
            'reg_date': article.reg_date
        })

    return jsonify({
        'meta': {
            'message': 'SUCCESS',
            'total_count': article_total_count
        },
        'response': result
    })


@rg_board.route('/<board_name>/cached', methods=['GET'])
def get_cached_article(board_name):
    pass


@rg_board.route('/<board_name>/cached/<int:article_cid>', methods=['GET'])
def get_cached_article_list(board_name, article_cid):
    pass


@rg_board.route('/<board_name>/popup', methods=['GET'])
def get_popup_article(board_name):
    board = board_info(board_name)
    if not board:
        return '', 404

    article_list = rg_board_content_orm().query.filter_by(
        rg_board_id=board.id).filter_by(
            popup=True).filter_by(
            status='visible')

    article_total_count = article_list.count()

    article_list = article_list.all()

    result = []
    for article in article_list:
        view_count = rg_board_content_access_log_orm().query.filter_by(
            rg_board_content_id=article.id).count()

        file_list_result = []
        file_list = rg_board_file_orm().query.filter_by(
            rg_board_content_id=article.id).filter_by(status='visible').all()

        for ufile in file_list:
            file_list_result.append({
                'id': ufile.id,
                'filename': ufile.filename,
                'filetype': ufile.filetype,
                'filesize_mb': ufile.filesize_mb,
                'reg_date': ufile.reg_date
            })

        tag_list_result = []
        tag_list = rg_board_content_tag_orm().query.filter_by(
            rg_board_content_id=article.id).filter_by(status='visible').all()
        for tag_obj in tag_list:
            tag_list_result.append(tag_obj.tag)

        guest_info = guest_orm().query.filter_by(id=article.guest_id).first()
        profile_image_url = ''
        if guest_info:
            if guest_info.division == 'FACEBOOK':
                profile_image_url = facebook_session_orm().query.filter_by(
                    guest_id=guest_info.id).first().profile_image_url

        result.append({
            'cid': article.cid,
            'title': article.title,
            'description': article.description,
            'guest_id': article.guest_id,
            'guest_name': article.guest_name,
            'guest_profile_image': profile_image_url,
            'lang_code': article.lang_code,
            'view_count': view_count,
            'is_notice': article.is_notice,
            'popup': article.popup,
            'in_private': article.in_private,
            'file_list': file_list_result,
            'tag_list': tag_list_result,
            'reg_date': article.reg_date
        })

    return jsonify({
        'meta': {
            'message': 'SUCCESS',
            'total_count': article_total_count
        },
        'response': result
    })


@rg_board.route('/<board_name>/tags', methods=['GET'])
def get_tags(board_name):
    board = board_info(board_name)
    if not board:
        return '', 404

    keyword = request.args.get(
        'keyword') if request.args.get('keyword') else ''

    if keyword:
        tags = rg_board_content_tag_orm().query.filter_by(
            rg_board_id=board.id).filter(
            rg_board_content_tag_orm.tag.ilike(
                text('"%' + request.args.get('keyword') + '%"'))
        ).filter_by(status='visible').distinct(rg_board_content_tag_orm.tag).all()
    else:
        tags = rg_board_content_tag_orm().query.filter_by(
            rg_board_id=board.id).filter_by(status='visible').distinct(rg_board_content_tag_orm.tag).all()

    if tags:
        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            },
            'response': list(set([r.tag for r in tags]))
        })
    else:
        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            },
            'response': []
        })


@rg_board.route('/<board_name>/<int:article_cid>')
def get_article(board_name, article_cid):
    board = board_info(board_name)
    if not board:
        return '', 404

    article = rg_board_content_orm().query.filter_by(
        rg_board_id=board.id).filter_by(
        cid=article_cid).filter_by(
        status='visible').first()

    if article:
        # prev, next article_cid 가져오기
        prev_article_query = rg_board_content_orm().query.filter_by(
            rg_board_id=board.id).filter_by(status='visible')
        prev_article = None
        if article.cid > 1:
            # prev
            for i in range(article.cid - 1, 1, -1):
                prev_article = prev_article_query.filter_by(cid=i).first()
                if prev_article:
                    break

        # next
        next_article = rg_board_content_orm().query.filter_by(
            rg_board_id=board.id).filter_by(status='visible').filter(rg_board_content_orm.cid > article.cid).first()

        file_list = rg_board_file_orm().query.filter_by(
            rg_board_content_id=article.id).filter_by(status='visible').all()

        file_result = []
        for ufile in file_list:
            file_result.append({
                'id': ufile.id,
                'filename': ufile.filename,
                'filetype': ufile.filetype,
                'filesize_mb': ufile.filesize_mb,
                'reg_date': ufile.reg_date
            })

        tag_list_result = []
        tag_list = rg_board_content_tag_orm().query.filter_by(
            rg_board_content_id=article.id).filter_by(status='visible').all()
        for tag_obj in tag_list:
            tag_list_result.append(tag_obj.tag)

        view_count_update(article.id)
        view_count = rg_board_content_access_log_orm().query.filter_by(
            rg_board_content_id=article.id).count()

        guest_info = guest_orm().query.filter_by(id=article.guest_id).first()
        if guest_info:
            if guest_info.division == 'FACEBOOK':
                profile_image_url = facebook_session_orm().query.filter_by(
                    guest_id=guest_info.id).first().profile_image_url
            else:
                profile_image_url = ''

        return jsonify({
            'meta': {
                'message': 'SUCCESS',
                'next_cid': next_article.cid if next_article is not None else None,
                'prev_cid': prev_article.cid if prev_article is not None else None
            },
            'response': {
                'cid': article.cid,
                'title': article.title,
                'description': article.description,
                'guest_id': article.guest_id,
                'guest_name': article.guest_name,
                'guest_profile_image': profile_image_url,
                'view_count': view_count,
                'file_list': file_result,
                'tag_list': tag_list_result,
                'in_private': article.in_private,
                'lang_code': article.lang_code,
                'is_notice': article.is_notice,
                'popup': article.popup,
                'reg_date': article.reg_date
            }
        })
    else:
        return '', 404


def view_count_update(article_id):
    # view count update
    ip_address = request.environ.get(
        'X-Forwarded-For', request.remote_addr)
    access_log = rg_board_content_access_log_orm()
    access_log.rg_board_content_id = article_id
    access_log.ip_address = ip_address
    if 'stored' in session:
        if g.serviceDomain in session['stored']:
            if 'guest_id' in session['stored'][g.serviceDomain]:
                access_log.guest_id = session['stored'][g.serviceDomain]['guest_id']
    db.session.add(access_log)
    db.session.commit()


@rg_board.route('/<board_name>/<int:article_cid>', methods=['DELETE'])
def delete_article(board_name, article_cid):
    board = board_info(board_name)
    if not board:
        return '', 404

    if 'guest_id' in session['stored'][g.serviceDomain]:
        article = rg_board_content_orm().query.filter_by(
            rg_board_id=board.id).filter_by(
            guest_id=session['stored'][g.serviceDomain]['guest_id']).filter_by(
            cid=article_cid).first()

        if article:
            article.status = 'hidden'
            db.session.add(article)

            # file delete
            for ufile in rg_board_file_orm().query.filter_by(rg_board_content_id=article.id).all():
                ufile.status = 'hidden'
                db.session.add(ufile)

            # tag delete
            for tag in rg_board_content_tag_orm().query.filter_by(rg_board_content_id=article.id).all():
                tag.status = 'hidden'
                db.session.add(tag)

            db.session.commit()

            return jsonify({
                'meta': {
                    'message': 'SUCCESS'
                },
                'response': None
            })
        else:
            return '', 404
    else:
        return '', 401


@rg_board.route('/<board_name>', methods=['POST'])
def post_article(board_name):
    board = board_info(board_name)
    if not board:
        return '', 404

    if 'guest_id' in session['stored'][g.serviceDomain]:
        last_article = rg_board_content_orm().query.filter_by(
            rg_board_id=board.id).order_by('-id').first()
        if last_article:
            last_cid = last_article.cid
        else:
            last_cid = 0

        article = rg_board_content_orm()
        article.rg_board_id = board.id
        article.title = request.get_json()['title']
        article.description = request.get_json()['description']
        article.guest_id = session['stored'][g.serviceDomain]['guest_id']
        article.guest_name = session['stored'][g.serviceDomain]['name']
        article.lang_code = request.get_json()['lang_code']
        article.in_private = request.get_json()['in_private']
        article.is_notice = request.get_json()['is_notice']
        article.popup = request.get_json()['popup']
        article.cid = last_cid + 1

        db.session.add(article)
        db.session.flush()

        if request.get_json()['file_list']:
            for tfile in request.get_json()['file_list']:
                post_file_info(article.id, tfile)

        tag_list = request.get_json(
        )['tag_list'] if 'tag_list' in request.get_json() else []
        for tag in tag_list:
            post_tag_info(board.id, article.id, tag)

        db.session.commit()

        return jsonify({
            'meta': {
                'message': 'SUCCESS',
                'cid': article.cid
            },
            'response': {
                'cid': article.cid
            }
        }), 201
    else:
        return '', 401


@rg_board.route('/<board_name>/modify/<int:article_cid>', methods=['GET', 'PUT'])
def update_article(board_name, article_cid):
    board = board_info(board_name)
    if not board:
        return '', 404

    if had_auth():
        article = rg_board_content_orm().query.filter_by(
            rg_board_id=board.id).filter_by(cid=article_cid).first()

        if article:
            if request.method == 'PUT':
                article.title = request.get_json()['title']
                article.description = request.get_json()['description']
                article.lang_code = request.get_json()['lang_code']
                article.in_private = request.get_json()['in_private']
                article.is_notice = request.get_json()['is_notice']
                article.popup = request.get_json()['popup']
                db.session.add(article)
                db.session.flush()

                if request.get_json()['file_list']:
                    file_list = rg_board_file_orm().query.filter_by(
                        rg_board_content_id=article.id).filter_by(
                            status='visible').all()
                    efile_id_list = [efile.id for efile in file_list]
                    for tfile in request.get_json()['file_list']:
                        ufile = post_file_info(article.id, tfile)
                        try:
                            efile_id_list.remove(ufile.id)
                        except:
                            continue

                    # 존재하는 파일 중, 삭제된 파일 숨기기
                    for id in efile_id_list:
                        etfile = rg_board_file_orm().query.filter_by(id=id).first()
                        if etfile:
                            etfile.status = 'hidden'
                            db.session.add(etfile)
                    db.session.flush()

                tag_list = request.get_json()['tag_list'] if 'tag_list' in request.get_json() else []
                oTag_list = rg_board_content_tag_orm().query.filter_by(
                    rg_board_content_id=article.id).filter_by(
                        status='visible').all()
                eTag_id_list = [eTag.id for eTag in oTag_list]
                for tag in tag_list:
                    uTag = post_tag_info(board.id, article.id, tag)
                    try:
                        eTag_id_list.remove(uTag.id)
                    except:
                        continue

                    for id in eTag_id_list:
                        etTag = rg_board_content_tag_orm().query.filter_by(id=id).first()
                        if etTag:
                            etTag.status = 'hidden'
                            db.session.add(etTag)
                    db.session.flush()

                db.session.commit()
                return jsonify({
                    'meta': {
                        'message': 'SUCCESS',
                        'cid': article.cid
                    }
                })
            elif request.method == 'GET':
                tag_list_result = []
                tag_list = rg_board_content_tag_orm().query.filter_by(
                    rg_board_content_id=article.id).all()
                for tag_obj in tag_list:
                    tag_list_result.append(tag_obj.tag)

                file_list = rg_board_file_orm().query.filter_by(
                    rg_board_content_id=article.id).filter_by(
                        status='visible').all()
                file_result = []
                for ufile in file_list:
                    file_result.append({
                        'id': ufile.id,
                        'filename': ufile.filename,
                        'filetype': ufile.filetype,
                        'filesize_mb': ufile.filesize_mb,
                        'reg_date': ufile.reg_date
                    })
                return jsonify({
                    'meta': {
                        'message': 'SUCCESS',
                        'cid': article.cid
                    },
                    'response': {
                        'cid': article.cid,
                        'title': article.title,
                        'description': article.description,
                        'file_list': file_result,
                        'tag_list': tag_list_result,
                        'in_private': article.in_private,
                        'lang_code': article.lang_code,
                        'is_notice': article.is_notice,
                        'popup': article.popup,
                        'reg_date': article.reg_date
                    }
                })
        else:
            return '', 404
    else:
        return '', 401


def post_tag_info(board_id, article_cid, tag):
    oTag = rg_board_content_tag_orm().query.filter_by(
        rg_board_content_id=article_cid).filter_by(tag=tag).first()
    if oTag:
        return oTag
    else:
        uTag = rg_board_content_tag_orm()
        uTag.rg_board_id = board_id
        uTag.rg_board_content_id = article_cid
        uTag.tag = tag

        db.session.add(uTag)
        db.session.flush()
        return uTag


def change_file_name(filename):
    d = datetime.utcnow() + timedelta(hours=9)
    unixtime = calendar.timegm(d.utctimetuple())
    regularated_filename = filename.replace(
        ' ', '').replace(r'[^a-zA-Z0-9\.]', '')

    if len(regularated_filename) > 50:
        regularated_filename = regularated_filename[len(
            regularated_filename)-51:]

    regularated_filename = str(unixtime).replace(
        '.', '_') + '_' + regularated_filename

    return regularated_filename


def post_file_info(rg_board_content_id, file_info):
    if 'id' in file_info and file_info['id']:
        ufile = rg_board_file_orm().query.filter_by(id=file_info['id']).first()
    else:
        ufile = rg_board_file_orm()
    ufile.filename = file_info['filename']
    ufile.rg_board_content_id = rg_board_content_id
    # To do: filetype check
    # filename change
    if 'path' in file_info:
        ufile.path = file_info['path']
    ufile.filetype = file_info['filename'].split('.')[-1]
    ufile.filesize_mb = file_info['filesize_mb']
    db.session.add(ufile)
    db.session.flush()
    return ufile


@rg_board.route('/<board_name>/file/<int:file_id>', methods=['DELETE'])
def delete_file(board_name, file_id):
    ufile = rg_board_file_orm().query.filter_by(id=file_id).first()

    if ufile:
        article = rg_board_content_orm().filter_by(
            id=ufile.rg_board_content_id).first()
        if article:
            if 'guest_id' in session['stored'][g.serviceDomain]:
                if article.guest_id == session['stored'][g.serviceDomain]['guest_id']:
                    ufile.status = 'hidden'
                    db.session.add(ufile)
                    db.session.commit()

                    return jsonify({
                        'meta': {
                            'message': 'SUCCESS'
                        },
                        'response': None
                    })
                else:
                    return '', 401
            else:
                return '', 401
    else:
        return '', 404


@rg_board.route('/<board_name>/file', methods=['POST'])
def file_upload(board_name):
    file_list = request.files.getlist('upload_files[]')

    def allowed_file(filename):
        return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    def is_image(filename):
        return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in IMAGE_EXTENSIONS

    def is_pdf(filename):
        return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() == 'pdf'

    save_result = []
    for file in file_list:
        if file and allowed_file(file.filename):
            # filename change
            filename = change_file_name(file.filename)
            path = FILE_PATH.format(request.headers['ServiceDomain'].replace(
                ".", "_").replace(":", "_").replace("/", "_"), board_name, filename)

            check_target_path = '/'
            splited_path = path.split("/")[1:-1]

            for p in splited_path:
                if len(p) > 0:
                    check_target_path += p
                    if not os.path.isdir(check_target_path):
                        os.mkdir(check_target_path, 755)
                    check_target_path += '/'

            # file save
            file.save(path)

            if is_pdf(file.filename):
                converted_image = pdf_to_image(path)
                if converted_image:
                    transformImage(converted_image)
                    save_result.append({
                        'path': converted_image,
                        'filename': file.filename.rsplit('.', 1)[0] + '_표지.jpg',
                        'filesize_mb': get_file_size(converted_image)
                    })

            # if image, transform
            if is_image(path):
                transformImage(path)

            save_result.append({
                'path': path,
                'filename': file.filename,
                'filesize_mb': get_file_size(path)
            })

    return jsonify({
        'meta': {
            'message': 'SUCCESS'
        },
        'response': save_result
    })


def pdf_to_image(path):
    try:
        from pdf2image import convert_from_path
        pages = convert_from_path(path)
        if len(pages) > 0:
            jpg_path = path.rsplit('.', 1)[0] + '.jpg'
            pages[0].save(jpg_path, 'JPEG')
            return jpg_path
        else:
            return None
    except:
        return None


def get_file_size(path):
    bytes_size = os.path.getsize(path)
    if bytes_size:
        return round(bytes_size / 1024 / 1024, 2)
    else:
        return 0.0


@rg_board.route('/file/<int:file_id>', methods=['GET'])
def getFile(file_id):
    file_info = rg_board_file_orm().query.filter_by(id=file_id).first()
    if file_info:
        return send_file(file_info.path)
    else:
        return '', 404


def transformImage(img_src):
    from PIL import Image
    im = Image.open(img_src)
    origin_size = im.size
    if origin_size[0] > 1920 or origin_size[1] > 1920:
        target_width = origin_size[0]
        target_height = origin_size[1]
        if origin_size[0] > 1920:
            target_width = 1920
            target_height = (1920 * origin_size[1]) / origin_size[0]
        if target_height > 1920:
            target_width = (1920 * target_width) / target_height
            target_height = 1920

        out = im.resize((int(target_width), int(target_height)))
        out.save(img_src)
