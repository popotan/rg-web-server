# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, request, Request, jsonify, session, redirect, g
import json
import urllib
import os
import re
from datetime import datetime, timedelta, time
import calendar

from passlib.hash import sha256_crypt

from sqlalchemy import and_, or_, text, asc, desc

from app import app, db

from app.model.domain.domain import domain_orm
from app.model.domain.allow_host import allow_host_orm

from app.model.guest.guest import guest_orm
from app.model.guest.guest_log import guest_log_orm
from app.model.guest.facebook_session import facebook_session_orm

from app.model.domain.domain_admin_user import domain_admin_user_orm
from app.model.domain.domain_admin_user_log import domain_admin_user_log_orm

from app.config.static import *

common_user = Blueprint('common_user', __name__)


@common_user.route('/join/<social>', methods=['POST'])
def join(social):
    if request.headers['ServiceDomain']:
        host_info = allow_host_orm().query.filter(
            allow_host_orm.host_name.ilike(text('"%' + request.headers['ServiceDomain'] + '%"'))).filter_by(status='usable').first()
    else:
        return '', 400

    if social == 'email':
        exist_guest = guest_orm().query.filter_by(
            domain_id=host_info.domain_id).filter_by(
            email=request.get_json()['email']).first()
        if exist_guest:
            return jsonify({
                'meta': {
                    'message': 'ALREADY_EXIST_EMAIL'
                }
            })
        else:
            guest = guest_orm()
            guest.domain_id = host_info.domain_id
            guest.division = 'EMAIL'
            guest.email = request.get_json()['email']
            guest.password = sha256_crypt.encrypt(
                request.get_json()['password'])
            guest.name = request.get_json()['name']
            db.session.add(guest)
            db.session.commit()

            return jsonify({
                'meta': {
                    'message': 'SUCCESS'
                }
            })
    else:
        pass


@common_user.route('/session/<social>', methods=['POST'])
def save_social_response(social):
    if request.headers['ServiceDomain']:
        host_info = allow_host_orm().query.filter(
            allow_host_orm.host_name.ilike(text('"%' + request.headers['ServiceDomain'] + '%"'))).filter_by(status='usable').first()
    else:
        return '', 400

    if social == 'facebook':
        fb = save_facebook_response(request.get_json())
        db.session.commit()

        auth = ['guest']
        if is_admin(host_info.domain_id, fb.guest_id):
            auth += ['admin']

        make_session(fb.guest_id, 'facebook', fb.email, auth, fb.name)
        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            }, 'response': {
                'guest_id': session['stored'][g.serviceDomain]['guest_id'],
                'name': fb.name,
                'email': fb.email,
                'auth': session['stored'][g.serviceDomain]['auth']
            }
        })
    elif social == 'email':
        guest = guest_orm().query.filter_by(
            email=request.get_json()['email']).filter_by(
                domain_id=host_info.domain_id).first()
        if guest:
            if sha256_crypt.verify(request.get_json()['password'], guest.password):
                if is_admin(host_info.domain_id, guest.id):
                    make_session(guest.id, 'EMAIL', guest.email,
                                 ['admin'], guest.name)
                else:
                    make_session(guest.id, 'EMAIL',
                                 guest.email, [], guest.name)
                return jsonify({
                    'meta': {
                        'message': 'SUCCESS'
                    }
                })
            else:
                return jsonify({
                    'meta': {
                        'message': 'PASSWORD_IS_NOT_MATCHED'
                    }
                })
        else:
            return jsonify({
                'meta': {
                    'message': 'USER_IS_NOT_EXIST'
                }
            })
    else:
        pass


def is_admin(domain_id, guest_id):
    admin = domain_admin_user_orm().query.filter_by(
        domain_id=domain_id).filter_by(guest_id=guest_id).first()
    if admin:
        return admin
    else:
        return None


def save_guest_info(division, email, name):
    if request.headers['ServiceDomain']:
        host_info = allow_host_orm().query.filter(
            allow_host_orm.host_name.ilike(text('"%' + request.headers['ServiceDomain'] + '%"'))).filter_by(status='usable').first()
    else:
        return '', 400

    guest_session = guest_orm().query.filter_by(
        domain_id=host_info.domain_id).filter_by(email=email).first()
    if not guest_session:
        guest_session = guest_orm()
        guest_session.domain_id = host_info.domain_id
    guest_session.email = email
    guest_session.name = name
    db.session.add(guest_session)
    db.session.commit()
    return guest_session


def save_facebook_response(info):
    fb_session = facebook_session_orm().query.filter_by(id=info['id']).first()
    if not fb_session:
        fb_session = facebook_session_orm()
        guest_session = save_guest_info(
            'facebook', info['email'], info['name'])
        fb_session.id = info['id']
        fb_session.guest_id = guest_session.id
    fb_session.name = info['name']
    # fb_session.birthday = info['user_birthday']
    fb_session.email = info['email']
    # fb_session.gender = info['user_gender']
    fb_session.profile_image_url = info['picture']['data']['url']
    # age = ''
    # if 'min' in info['user_age_range']:
    #     age += str(info['user_age_range']['min'])
    # age += '-'
    # if 'max' in info['user_age_range']:
    #     age += str(info['user_age_range']['max'])
    # fb_session.age = age
    db.session.add(fb_session)
    return fb_session


def make_session(guest_id, division, email, auth=[], name=''):
    session.modified = True
    if not 'stored' in session:
        session['stored'] = dict()
    if not g.serviceDomain in session['stored']:
        session['stored'][g.serviceDomain] = dict()
    session['stored'][g.serviceDomain]['guest_id'] = guest_id
    session['stored'][g.serviceDomain]['division'] = division
    session['stored'][g.serviceDomain]['email'] = email
    if name:
        session['stored'][g.serviceDomain]['name'] = name
    else:
        session['stored'][g.serviceDomain]['name'] = ''

    if auth:
        session['stored'][g.serviceDomain]['auth'] = auth


@common_user.route('/session', methods=['GET', 'DELETE'])
def session_work():
    if request.method == 'GET':
        if not 'stored' in session:
            session['stored'] = dict()
        if not g.serviceDomain in session['stored']:
            session['stored'][g.serviceDomain] = dict()

        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            },
            'response': {
                'guest_id': session['stored'][g.serviceDomain].get('guest_id', None),
                'email': session['stored'][g.serviceDomain].get('email', None),
                'division': session['stored'][g.serviceDomain].get('division', None),
                'name': session['stored'][g.serviceDomain].get('name', None),
                'auth': session['stored'][g.serviceDomain].get('auth', None)
            }
        })
    elif request.method == 'DELETE':
        session['stored'][g.serviceDomain] = {}
        return jsonify({
            'meta': {
                'message': 'SUCCESS'
            }
        })
