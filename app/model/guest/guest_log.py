# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class guest_log_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'guest_log'
    id = Column(Integer, primary_key=True, unique=True)
    guest_id = Column(Integer)
    log_type = Column(String(45))
    description = Column(String(2000))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
