# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class facebook_session_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'facebook_session'
    id = Column(String(45), primary_key=True, unique=True)
    guest_id = Column(Integer, unique=True)
    email = Column(String(200))
    name = Column(String(45))
    age = Column(String(45))
    birthday = Column(String(45))
    gender = Column(String(45))
    profile_image_url = Column(String(1000))
    access_token = Column(String(1000))
    refresh_token = Column(String(1000))
    expires_in = Column(DateTime)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
