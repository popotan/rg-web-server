# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db
from app.model.guest.facebook_session import facebook_session_orm
from app.model.guest.guest_log import guest_log_orm

from datetime import datetime, timedelta


class guest_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'guest'
    id = Column(Integer, primary_key=True, unique=True)
    domain_id = Column(Integer)
    division = Column(String(45), default='FACEBOOK')
    email = Column(String(200))
    password = Column(String(255))
    name = Column(String(45))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    # facebook_session = db.relationship(facebook_session_orm, backref="guest")
    # guest_log = db.relationship(guest_log_orm, backref="guest")