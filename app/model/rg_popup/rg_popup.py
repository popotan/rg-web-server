# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class rg_popup_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'rg_popup'
    id = Column(Integer, primary_key=True, unique=True)
    domain_id = Column(Integer)
    title = Column(String(45))
    description = Column(String)
    exposure_path = Column(String)
    exposure_start_date = Column(DateTime)
    exposure_end_date = Column(DateTime)
    exposure_type = Column(String)
    exposure_size_width = Column(Integer)
    exposure_size_height = Column(Integer)
    lang_code = Column(String)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
