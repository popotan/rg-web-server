# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class allow_host_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'allow_host'
    id = Column(Integer, primary_key=True, unique=True)
    domain_id = Column(Integer)
    status = Column(String(45))
    host_name = Column(String(255))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
