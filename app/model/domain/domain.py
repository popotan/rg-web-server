# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class domain_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'domain'
    id = Column(Integer, primary_key=True, unique=True)
    name = Column(String(45))
    status = Column(String(45))
    hashed_id = Column(String(1000), unique=True)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))