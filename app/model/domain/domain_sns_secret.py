# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class domain_sns_secret_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'domain_sns_secret'
    id = Column(Integer, primary_key=True, unique=True)
    domain_id = Column(Integer)
    sns = Column(String(45))
    key = Column(String(45))
    value = Column(String(1000))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
