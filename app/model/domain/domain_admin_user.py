# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class domain_admin_user_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'domain_admin_user'
    id = Column(Integer, primary_key=True, unique=True)
    domain_id = Column(Integer)
    guest_id = Column(Integer)
    status = Column(String(45))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))