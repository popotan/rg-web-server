# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, Float
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class rg_board_file_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'rg_board_file'
    id = Column(Integer, primary_key=True, unique=True)
    rg_board_content_id = Column(Integer)
    filename = Column(String(255))
    path = Column(String(2000))
    filetype = Column(String(45))
    filesize_mb = Column(Float)
    status = Column(String(45), default='visible')
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
