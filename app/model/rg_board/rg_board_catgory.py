# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta
from .rg_board_lang_code import rg_board_lang_code_orm


class rg_board_category_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'rg_board_category'
    id = Column(Integer, primary_key=True, unique=True)
    domain_id = Column(Integer)
    name = Column(String(200))
    visibility = Column(Boolean)
    style = Column(String(45))
    category = Column(Integer)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def get_lang_code(self):
        lang_code_list = rg_board_lang_code_orm().query.filter_by(rg_board_id=self.id).all()
        result = []
        for lang in lang_code_list:
            result.append({
                'lang_code': lang.lang_code,
                'aka': lang.aka
            })
        return result
