# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class rg_board_reply_history_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'rg_board_reply_history'
    id = Column(Integer, primary_key=True, unique=True)
    rg_board_reply_id = Column(Integer)
    description = Column(String)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
