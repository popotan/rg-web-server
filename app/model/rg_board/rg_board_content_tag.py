# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class rg_board_content_tag_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'rg_board_content_tag'
    id = Column(Integer, primary_key=True, unique=True)
    rg_board_id = Column(Integer)
    rg_board_content_id = Column(Integer)
    tag = Column(String(100))
    status = Column(String(45), default='visible')
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
