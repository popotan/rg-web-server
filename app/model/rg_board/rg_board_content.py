# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class rg_board_content_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'rg_board_content'
    id = Column(Integer, primary_key=True, unique=True)
    cid = Column(Integer)
    rg_board_id = Column(Integer)
    title = Column(String(255))
    description = Column(String(20000))
    status = Column(String(45), default='visible')
    guest_id = Column(Integer)
    guest_name = Column(String(45))
    lang_code = Column(String(45))
    in_private = Column(Boolean, default=False)
    is_notice = Column(Boolean, default=False)
    popup = Column(Boolean, default=False)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
