# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from app import db

from datetime import datetime, timedelta


class rg_board_lang_code_orm(db.Model):
    __bind_key__ = 'rg_web'
    __tablename__ = 'rg_board_lang_code'
    id = Column(Integer, primary_key=True, unique=True)
    rg_board_id = Column(Integer)
    lang_code = Column(String(45))
    aka = Column(String(200))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
