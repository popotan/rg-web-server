# MYSQL
SQLALCHEMY_TRACK_MODIFICATIONS = True

# MySQL
# Uncomment the below string if you are using MySQL or MariaDB
SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:withasia2018@localhost/rg_web?charset=utf8&use_unicode=0"
SQLALCHEMY_BINDS = {
    'rg_web': "mysql+pymysql://root:withasia2018@localhost/rg_web?charset=utf8&use_unicode=0"
}
