FILE_PATH = '/home/acc/www/static/{}/{}/{}'
ALLOWED_EXTENSIONS = set(['pdf', 'jpeg', 'png', 'jpg', 'gif', 'hwp',
                          'docx', 'doc', 'ppt', 'pptx', 'csv', 'xlsx', 'xls', 'txt'])
IMAGE_EXTENSIONS = set(['jpeg', 'jpg', 'png', 'gif'])
# {}.format('도메인명/폴더명/파일명')
