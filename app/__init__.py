# _*_ coding: utf-8 _*_
from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

from app.config import database
from datetime import timedelta

import os

app = Flask(__name__, template_folder='template', static_url_path='/static')
app.secret_key = 'rg_web'

app.config.from_object(database)
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

# 쿠키관련 설정
app.config.update(
    SESSION_TYPE='sqlalchemy',
    SESSION_COOKIE_NAME='rg_web',
    SESSION_COOKIE_HTTPONLY=False,
    SESSION_COOKIE_SECURE=False,
    PERMANENT_SESSION_LIFETIME=timedelta(seconds=600),
    SESSION_REFRESH_EACH_REQUEST=True
)
Session(app)

bcrypt = Bcrypt(app)


@app.route('/static/<path:path>')
def send_js(path):
    """
            static 파일들을 템플릿상에서 url_for 없이 사용하기 위해 라우팅 제공
    """
    return send_from_directory('static/', path)


# g variables
@app.before_request
def set_g_variables():
    if 'ServiceDomain' in request.headers:
        g.serviceDomain = request.headers['ServiceDomain'].replace(
            ":", "_").replace(".", "_")
    else:
        g.serviceDomain = None
        
    if not 'stored' in session.keys():
        session['stored'] = dict()
    if not g.serviceDomain in session['stored'].keys():
        session['stored'][g.serviceDomain] = dict()


@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


from app.controller.common_user import common_user
from app.controller.rg_board import rg_board
from app.controller.rg_popup import rg_popup
from app.controller.etc import etc

custom_url_prefix = '/api'

app.register_blueprint(common_user, url_prefix=custom_url_prefix+'/cuser')
app.register_blueprint(rg_board, url_prefix=custom_url_prefix+'/board')
app.register_blueprint(rg_popup, url_prefix=custom_url_prefix+'/popup')
app.register_blueprint(etc, url_prefix=custom_url_prefix+'/etc')
