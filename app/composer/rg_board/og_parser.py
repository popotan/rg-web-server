import opengraph

class OgParser(object):
    TARGET_URL = ''
    OGP = None

    def __init__(self, url):
        self.TARGET_URL = url
        self.OGP = opengraph.OpenGraph(url=self.TARGET_URL)

    @property
    def json(self):
        if self.OGP:
            return {
                'title' : self.OGP.title,
                'url' : self.OGP.url,
                'image' : self.OGP.image,
                'description' : self.OGP.description
            }
        else:
            return None