# -*- coding:utf-8 -*-
 
import smtplib
from email.mime.text import MIMEText

class MailSender(object):
    MSG = None
    SMTP = None

    def __init__(self, id, password):
        self.SMTP = smtplib.SMTP('smtp.live.com', 587)
        self.SMTP.ehlo()      # say Hello
        self.SMTP.starttls()  # TLS 사용시 필요
        self.SMTP.login(id, password)

    def write(self, mail_from, mail_to, subject, msg):
        self.MSG = MIMEText(msg)
        self.MSG['Subject'] = subject
        self.MSG['To'] = mail_to
        return self

    def send(self, mail_from, mail_to):
        self.SMTP.sendmail(mail_from, mail_to, self.MSG.as_string())
        self.SMTP.quit()