from functools import wraps
from flask import session, g


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'is_logged_in' in session['stored'][g['serviceDomain']].keys():
            if session['stored'][g['serviceDomain']]['is_logged_in']:
                return f(*args, **kwargs)
        return ('', 401)
    return decorated_function


def login_required_admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'division' in session['stored'][g['serviceDomain']].keys():
            if 'auth' in session['stored'][g['serviceDomain']]['auth']:
                return f(*args, **kwargs)
        return ('', 401)

    return decorated_function


def required_admin_or_cetificated(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
